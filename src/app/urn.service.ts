import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

@Injectable()
export class UrnService {
  private _urnColorsUrl = './api/urn-colors.json';
  private _urnStylesUrl = './api/urn-styles.json';

  private _urnNoImageURL = '/assets/images/urns/classic.base.jpg';

  constructor(private _http: HttpClient) { }

  getUrnColors(): Observable<string[]> {
    return this._http.get<string[]>(this._urnColorsUrl)
      .do(data => console.log('Got Urn Colors: ' + JSON.stringify(data) ))
      .catch(this.handleError);
  }

  getUrnStyles(): Observable<string[]> {
    return this._http.get<string[]>(this._urnStylesUrl)
      .do(data => console.log('Got Urn Styles: ' + JSON.stringify(data) ))
      .catch(this.handleError);
  }

  GetUrnPictureURL(urnColor: string,  urnStyle: string): string {
    if (!urnStyle && !urnColor) {
      return this._urnNoImageURL;
    }
    let urnPicUrl = '/assets/images/urns/';
    urnPicUrl += urnStyle  ? urnStyle.toLocaleLowerCase() : 'classic';
    urnPicUrl += '.';
    urnPicUrl += urnColor ? urnColor.toLocaleLowerCase() : 'base';
    urnPicUrl += '.jpg';
    return urnPicUrl;
  }

  private handleError(err: HttpErrorResponse) {
    console.log(err.message);
    return Observable.throw(err.message);
  }

}
