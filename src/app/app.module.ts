import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AccordionModule } from 'ngx-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { ConfigureComponent } from './configure/configure.component';
import { UrnService } from './urn.service';

@NgModule({
  declarations: [
    AppComponent,
    ConfigureComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AccordionModule.forRoot()
  ],
  providers: [UrnService],
  bootstrap: [AppComponent]
})
export class AppModule { }
