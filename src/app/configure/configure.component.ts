import { Component, OnInit, OnChanges, EventEmitter, Output } from '@angular/core';
import { NgFor } from '@angular/common';
import { UrnService } from '../urn.service';

@Component({
  selector: 'app-configure',
  templateUrl: './configure.component.html',
  styleUrls: ['./configure.component.css']
})
export class ConfigureComponent implements OnInit {
  errorMessage: string;
  public panelClass = 'auto-size-accordion-panel';
  // urnColors: string[] = ['mystic_blue', 'emerald', 'cashmere_gray', 'rose', 'ebony'];
  // urnStyles: string[] = ['Grecian', 'Classic', 'small_cube'];
  urnColors: string[];
  urnStyles: string[];

  urnStyle: string;
  urnColor: string;

  @Output() urnStylePicked: EventEmitter<string> = new EventEmitter<string>();
  @Output() urnColorPicked: EventEmitter<string> = new EventEmitter<string>();


  constructor(private _urnService: UrnService) {
  }

  ngOnInit(): void {
    this._urnService.getUrnColors()
      .subscribe(urnColors => {
        this.urnColors = urnColors;
      }, error => this.errorMessage = <any>error);
    this._urnService.getUrnStyles()
      .subscribe(urnStyles => {
        this.urnStyles = urnStyles;
        this.urnStyle = this.urnStyles[0];
      }, error => this.errorMessage = <any>error);
  }

  onStyleClick(urnStyleIn: string): void {
    this.urnStyle = urnStyleIn;
    console.log(this.urnStyle);
    this.urnStylePicked.emit(this.urnStyle);
  }

  onColorClick(urnColor: string): void {
    this.urnColor = urnColor;
    console.log(this.urnColor);
    this.urnColorPicked.emit(this.urnColor);
  }

}
