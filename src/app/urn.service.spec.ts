import { TestBed, inject } from '@angular/core/testing';

import { UrnService } from './urn.service';

describe('UrnService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UrnService]
    });
  });

  it('should be created', inject([UrnService], (service: UrnService) => {
    expect(service).toBeTruthy();
  }));
});
