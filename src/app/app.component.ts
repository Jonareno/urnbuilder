import { Component } from '@angular/core';
import { UrnService } from './urn.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  urnColor: string;
  urnStyle: string;
  urnNoImageURL = '/assets/images/urns/classic.base.jpg';
  urnImageURL: string = this.urnNoImageURL;
  title = 'UrnBuilder';

  constructor(private _urnService: UrnService) {

  }

  onUrnStylePicked(style: string): void {
    this.urnStyle = style;
    this.updateUrnImageURL();
  }

  onUrnColorPicked(color: string): void {
    this.urnColor = color;
    this.updateUrnImageURL();
  }

  updateUrnImageURL(): void {
    this.urnImageURL = this._urnService.GetUrnPictureURL(this.urnColor, this.urnStyle);
  }
}
